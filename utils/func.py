import overpy
import pandas as pd
import geopandas as gpd


def remove_null_values(obj):
    """
    function to remove null values from a dict
    """
    if isinstance(obj, dict):
        return {
            key: val 
            for key, val in ((key,remove_null_values(val)) for key,val in obj.items())
            if val
        }
    if isinstance(obj, list):
        return [val for val in map(remove_null_values, obj) if val]
    return obj

def result_search_overpy(area_id:int,tags:dict, timeout=25):
    str_tags = ''.join([f'["{key}"="{value}"]' for key,value in tags.items()])
    query = f"""[out:json][timeout:{timeout}];
    //+fetch+area+“Tunisia”+to+search+in
    // {{geocodeArea:Tunisia}}->.searchArea;
    area(id:{area_id})->.searchArea;
    //+gather+results
    node{str_tags}(area.searchArea);
    out;"""
    api = overpy.Overpass()
    result = api.query(query)
    return result

def osm_to_gdf(result:overpy.Result, crs='epsg:4326'):
    """
    Function to transform the result from overpy.result to geodataframe
    """
    locations = [{'id':node.id, 'lat':node.lat, 'lon':node.lon } \
                 for node in result.nodes]
    data = [node.tags for node in result.nodes]
    _ = [data[item].update(locations[item]) for item in range(len(data))]
    df = pd.DataFrame(data)
    gdf = gpd.GeoDataFrame(df, 
                           geometry=gpd.points_from_xy(x=df.lat, y=df.lon))
    gdf = gdf.drop(columns=['lat','lon'])
    gdf = gdf.set_crs(crs)
    return gdf

if __name__ == '__main__':
    pass